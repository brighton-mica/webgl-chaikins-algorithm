const WIDTH = 800;
const HEIGHT = 800;

points = [];
curvePoints = [];
activePoint = -1;
iterations = 2;

class Point {
    constructor(x, y, diameter) {
        this.x = x;
        this.y = y;
        this.diameter = diameter;
        this.radius = diameter / 2;
    }

    draw(name) {

        if (this.hover()) {
            push();
            fill(200);
            ellipse(this.x, this.y, this.diameter);
            pop();
            text(name, this.x - 7, this.y + 3);
        } else {
            push();
            fill(247);
            ellipse(this.x, this.y, this.diameter);
            pop();
            text(name, this.x - 7, this.y + 3);
        }

    }

    hover() {
        return dist(mouseX, mouseY, this.x, this.y) < this.radius;
    }

    static getAngle(p1, p2) {
        theta = (180/PI) * atan((p2.y - p1.y) / (p2.x - p1.x));

        if (p2.x < p1.x) {
            x1 = p1.x - p1.radius*cos((PI/180)*theta);
            y1 = p1.y - p1.radius*sin((PI/180)*theta);
            return [x1, y1];
        } else {
            x1 = p1.x + p1.radius*cos((PI/180)*theta);
            y1 = p1.y + p1.radius*sin((PI/180)*theta);
            return [x1, y1];
        }
    }
}

// Events

function changeIterations(n) {
    iterations = n;
}

function addPoint() {
    console.log("SDF");
    points.push(new Point(WIDTH / 2, HEIGHT / 2, 30));
}

function reset() {
    points = [new Point(100, 300, 30), new Point(100, 100, 30), new Point(300, 100, 30)];
}

// P5 code

function mousePressed() {
    for (i = 0; i < points.length; i++) {
        if (points[i].hover()) {
            activePoint = i;
            break;
        }
    }
}

function mouseDragged() {
    if (activePoint != -1 && mouseX < (WIDTH - points[activePoint].radius) && mouseX > points[activePoint].radius && mouseY < (HEIGHT - points[activePoint].radius) && mouseY > points[activePoint].radius) {
        points[activePoint].x = mouseX;
        points[activePoint].y = mouseY;
    }
}

function mouseReleased() {
    activePoint = -1;
}

function setup() {
    cnv = createCanvas(WIDTH, HEIGHT);
    cnv.parent("canvas-wrapper");
    cnv.mouseOut(() => activePoint = -1);
    points = [new Point(100, 300, 30), new Point(100, 100, 30), new Point(300, 100, 30)];
}
  
function draw() {
    background(247);

    // Draw line segments
    for (i = 0; i < points.length - 1; i++) {
        theta = (180/PI) * atan((points[i+1].y - points[i].y) / (points[i+1].x - points[i].x));

        if (points[i+1].x < points[i].x) {
            push();
            x1 = points[i].x - points[i].radius*cos((PI/180)*theta);
            y1 = points[i].y - points[i].radius*sin((PI/180)*theta);
            x2 = points[i+1].x + points[i+1].radius*cos((PI/180)*theta);
            y2 =  points[i+1].y + points[i+1].radius*sin((PI/180)*theta);
            stroke(200);
            line(x1, y1, x2, y2);
            pop();
        } else {
            push();
            x1 = points[i].x + points[i].radius*cos((PI/180)*theta);
            y1 = points[i].y + points[i].radius*sin((PI/180)*theta);
            x2 = points[i+1].x - points[i+1].radius*cos((PI/180)*theta);
            y2 =  points[i+1].y - points[i+1].radius*sin((PI/180)*theta);
            stroke(200);
            line(x1, y1, x2, y2);           
            pop();
        }
    }

    // Populate curvedPoints with the points of the curved line using Chaikin's Algorithm
    currentIteration = 0;
    u = createVector(points[0].x, points[0].y);
    v = createVector(points[1].x, points[1].y);
    w = createVector(points[2].x, points[2].y);
    curvePoints = points;
    while (currentIteration < iterations) {
        newCurvePoints = [];

        for (i = 0; i < curvePoints.length - 1; i++) {
            u = createVector(curvePoints[i].x, curvePoints[i].y);
            v = createVector(curvePoints[i+1].x, curvePoints[i+1].y);
            q = p5.Vector.add(p5.Vector.mult(u, 3/4), p5.Vector.mult(v, 1/4));
            r = p5.Vector.add(p5.Vector.mult(u, 1/4), p5.Vector.mult(v, 3/4));
            newCurvePoints.push(q);
            newCurvePoints.push(r);
        }

        curvePoints = newCurvePoints;
        currentIteration++;
    }

    // Draw beginning, middle, and end of curved line
    [xStart, yStart] = Point.getAngle(points[0], { x : curvePoints[0].x, y : curvePoints[0].y });
    line(xStart, yStart, curvePoints[0].x, curvePoints[0].y);
    for (i = 0; i < curvePoints.length - 1; i++) {
        line(curvePoints[i].x, curvePoints[i].y, curvePoints[i+1].x, curvePoints[i+1].y);
    }
    [xEnd, yEnd] = Point.getAngle(points[points.length - 1], { x : curvePoints[curvePoints.length - 1].x, y : curvePoints[curvePoints.length - 1].y });
    line(curvePoints[curvePoints.length - 1].x, curvePoints[curvePoints.length - 1].y, xEnd, yEnd);

    // Draw points
    for (i = 0; i < points.length; i++) {
        points[i].draw("p" + i);
    }
}